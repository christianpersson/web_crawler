var jsdom = require("jsdom");
var MongoClient = require("mongodb").MongoClient;


var insertDocuments = function(objs, db, callback) {
    var collection = db.collection('items');
    collection.insert(objs, function(err, result) {
        callback(result);
    });
}


var objs = [];
jsdom.env({
    url: "http://www.blocket.se/vasterbotten?ca=2&w=1&md=li",
    scripts: ["http://code.jquery.com/jquery.js"],
    done: function (errors, window) {
        var $ = window.$;
        $("#item_list>.item_row").each(function (data) {

            var link = $(this).find('.desc>a').text();
            var url = $(this).find('.desc>a').attr('href');
            var price = $(this).find('.list_price').text().trim().replace("\n", "").replace("\t", "");
            var cat = $(this).find('.cat_geo>.textlist_left>a').text();
            var loc = $(this).find('.cat_geo>.textlist_right>.list_subarea>a').text();

            var obj = {};
            obj.title = link;
            obj.url = url;
            obj.price = price;
            obj.cat = cat;
            obj.loc = loc;
            objs.push(obj);
        });

        var url = 'mongodb://localhost:27017/blocket';
        MongoClient.connect(url, function(err, db) {
            console.log("Connected correctly to server");
            insertDocuments(objs, db, function() {
                db.close();
            });
        });
    }
});

